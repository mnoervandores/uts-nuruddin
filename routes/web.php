<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VehicleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/kendaraan',[VehicleController::class, 'index'])->name('kendaraan');
Route::get('/tambahkendaraan',[VehicleController::class, 'tambahkendaraan'])->name('tambahkendaraan');
Route::post('/insertdata',[VehicleController::class, 'insertdata'])->name('insertdata');
Route::get('/tampilkandata/{id}',[VehicleController::class, 'tampilkandata'])->name('tampilkandata');
Route::post('/updatedata/{id}',[VehicleController::class, 'updatedata'])->name('updatedata');
Route::get('/delete/{id}',[VehicleController::class, 'delete'])->name('delete');
Route::get('/info/{id}',[VehicleController::class, 'info'])->name('info');
