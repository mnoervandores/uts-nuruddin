<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Seeder;

class VehicleSeeder extends Seeder
{
    public function run()
    {
        DB::table('vehicles')->insert([
            'nama' => 'tuyul arab',
            'jeniskendaraan' => 'roda dua',
            'merkkendaraan' => 'ontajets',
            'notelpon' => '99926283621',
            'expiredkartu' => '12 oktober 2051',
        ]);
    }


}

