<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use Facade\FlareClient\View;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $data = Vehicle::all();
        return view('datakendaraan',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambahkendaraan(){
        return view('tambahdata');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertdata(Request $request){
        // dd($request->all());
        Vehicle::create($request->all());
        return redirect()->route('kendaraan');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function tampilkandata($id)
    {
        $data = Vehicle::find($id);
        // dd($data);
        return view('tampildata',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function updatedata(Request $request, $id)
    {
        $data = Vehicle::find($id);
        $data->update($request->all());
        return redirect()->route('kendaraan');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function info($id)
    {
        $info = Vehicle::find($id);
        return view('info',compact('info'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Vehicle::find($id);
        $data->delete();
        return redirect()->route('kendaraan');
    }
}
